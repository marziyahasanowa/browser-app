package com.example.browserapp

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import android.Manifest
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {
    lateinit var searchButton:Button
    lateinit var webView: WebView
    lateinit var urlText: EditText
    private val INTERNET_PERMISSION_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //WebView
        webView = findViewById(R.id.webView)
        webView.webViewClient = object : WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }
        }
        webView.webChromeClient = object : WebChromeClient(){
            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                if (!title.isNullOrEmpty()) {
                    supportActionBar?.title = title
                } else {
                    supportActionBar?.title = getString(R.string.app_name)
                }
            }
        }

        searchButton = findViewById(R.id.button_search)
        urlText = findViewById(R.id.textInputEditText)


        //Toolbar Title
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = getString(R.string.app_name)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        setSupportActionBar(toolbar)

        searchButton.setOnClickListener {
            val url = urlText.text.toString()
            if (url.isNotEmpty()){
                webView.loadUrl(url)
            }
        }
        enterKeySearch()
    }


    //Options Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val backgroundColor = when(item.itemId) {
            R.id.green -> R.color.green
            R.id.blue -> R.color.blue
            R.id.violet -> R.color.violet
            R.id.brown -> R.color.brown
            R.id.orange -> R.color.orange
            R.id.yellow -> R.color.yellow
            else -> R.color.gray
        }
        searchButton.setBackgroundColor(ContextCompat.getColor(this, backgroundColor))
        return super.onOptionsItemSelected(item)
    }


    //Enter Key -> Search Button
    private fun enterKeySearch() {
        urlText.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                // Perform search action here
                searchButton.performClick()
                return@OnEditorActionListener true
            }
            false
        })
    }
}

